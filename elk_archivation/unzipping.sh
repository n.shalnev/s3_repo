# bin/bash

# Дата нужного архива задается при запууске скрипта в форма "yyyy.mm.dd"
# unzipping.sh 2019.04.05


# Переменные
IP="IP"
PORT="9200"
LOG='/var/log/elasticsearch/elasticsearch_unzipping.log'
ARCHIV_DIR='/srv/index_archive'
FTP_ARCH_DIR='/opt/index_archive'
INDICES=$(curl -X GET "$IP:$PORT/_cat/indices" | grep "$1" | awk '{ print $2 }')
MS_NODE="mk-elk-1"

HOST=hostname
USER=user
PASSWORD=passwd

# Проверяем что нужная дата указана
if [ -z $1 ]
then
    echo "Не указана нужная дата архива. Пример запуска скрипта с датой 'unzipping.sh 2019.04.05'"
    exit
else

    # Забрать архив с ftp
    sshpass -p "$PASSWORD" scp $USER@$HOST:$FTP_ARCH_DIR/$1.tar.gz $ARCHIV_DIR

    # Проверяем наличие архива на заданную дату
    if ! [ -f $ARCHIV_DIR/$1.tar.gz ]
    then
        echo "Указанная дата не найдена"
        exit
    else
        # Разархивируем файлы в папки индексов
        tar -xvf $ARCHIV_DIR/$1.tar.gz -C /srv/lib/elasticsearch/nodes/0/indices/
        curl -X PUT "$IP:$PORT/_cluster/settings" -H 'Content-Type: application/json' -d' { "persistent": { "cluster.routing.allocation.enable": "true" } }' > /dev/null 2>&1
        for i in $INDICES
            do
            # Открываем индекс
            curl -X POST "$IP:$PORT/$i/_open"
            str=$(curl -X GET "$IP:$PORT/_cluster/state/routing_table/$i" | jq '[.["routing_table"]["indices"][]["shards"]]' | awk '{print $1}' | sort | sed s/[^0-9]//g)
            for s in $str
                do
                st="curl -X GET \"$IP:$PORT/_cluster/state/routing_table/$i\" | jq '[.[\"routing_table\"][\"indices\"][][\"shards\"][\"$s\"]]' | grep UNASSIGNED"
                STATE=`eval $st`
                if [[ -n "$st" ]];
                then
                    RESET_SHARD="/usr/bin/curl -X POST \"$IP:$PORT/_cluster/reroute\" -H \"Content-Type: application/json\" -d' { \"commands\" : [ { \"allocate_empty_primary\" : { \"index\" : \"$i\", \"shard\" : $s, \"node\" : \"$MS_NODE\", \"accept_data_loss\" : true } } ] }'"
                    eval $RESET_SHARD > /dev/null 2>&1
                    echo "Reset shards $s for indices $i"
                else
                echo "Shards $s indices $i is STARTED"
                fi
            done
            echo "Индекс $i открыт"
        done
    fi
    curl -X PUT "$IP:$PORT/_cluster/settings" -H 'Content-Type: application/json' -d' { "persistent": { "cluster.routing.allocation.enable": "all"} }' > /dev/null 2>&1
    # Удаляем архив
    rm -rf $ARCHIV_DIR/$1.tar.gz

fi