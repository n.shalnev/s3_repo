# bin/bash

# Удаляем архивы старше года
DAYSAGO=`date --date="365 days ago" +%Y%m%d`
ALLLINES=`ls /opt/index_archive/`

echo "$ALLLINES" | while read LINE
do
  FORMATEDLINE=`echo $LINE |  | sed -e 's/[^0-9]//g' ` 
  if [ "$FORMATEDLINE" -le "$DAYSAGO" ]
  then
    echo "Delete $FORMATEDLINE"
    rm -rf /opt/index_archive/$FORMATEDLINE
  fi
done