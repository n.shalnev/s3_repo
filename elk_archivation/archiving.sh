# bin/bash

set -e

# Переменные
IP="IP"
PORT="9200"
ARCHIV_DATA=$(date --date="2 day ago" +%Y.%m.%d)
LOG='/var/log/elasticsearch/elasticsearch_archive.log'
DATE='date'
ARCHIV_DIR='/srv/index_archive'
FTP_ARCH_DIR='/opt/index_archive'

HOST="IP"
USER="ftpuser"
PASSWORD="passwd"

# Проверяем что папка для архивов существует, если нет создаем
if ! [ -d $ARCHIV_DIR ]; then
  mkdir -p $ARCHIV_DIR
fi

# Получаем список индексов старше N дней
INDICES=$(curl -X GET "$IP:$PORT/_cat/indices" | grep "$ARCHIV_DATA"  | grep -v ".reporting*" | awk '{ print $3 }')

#Проверим, не пустой ли список
TEST_INDICES=`echo $INDICES | grep -q -i "error" && echo 1 || echo 0`
 
if [ $TEST_INDICES == 1 ]
then
  echo "$DATE Не найдено индексов для обработки"
  exit
else

  # Архивируем данные индексов
  tar -cvzf $ARCHIV_DIR/$ARCHIV_DATA.tar.gz -C /srv/lib/elasticsearch/nodes/0/indices/ -- $INDICES

  # Отправка архивов на ftp
  sshpass -p "$PASSWORD" scp -C $ARCHIV_DIR/$ARCHIV_DATA.tar.gz $USER@$HOST:$FTP_ARCH_DIR
  echo "$ARCHIV_DATA.tar.gz загружен в storage"

  # cd $ARCHIV_DIR
  # ftp -int $HOST <<EOF
  # user $USER $PASSWORD
  # cd archive_ftp
  # put $ARCHIV_DIR/$ARCHIV_DATA.tar.gz
  # bye
  # EOF

  # Удаляем скопированный архив
  rm -rf $ARCHIV_DIR/$ARCHIV_DATA.tar.gz

  # Составляем цикл для очистки каждого индекса в массиве $INDICES
  for i in $INDICES
      do
          rm -rf /srv/lib/elasticsearch/nodes/0/indices/$i/*
  done
fi

# Удаляем данные из всех папок старше нужной даты
DAYSAGO=`date --date="2 days ago" +%Y%m%d`
ALLLINES=`/usr/bin/curl -s -XGET $IP:$PORT/_cat/indices?v`

echo "$ALLLINES" | while read LINE
do
  FORMATEDLINE=`echo $LINE | awk '{ print $2 }' | awk -F'-' '{ print $3 }' | sed 's/\.//g' `
  var=`(($FORMATEDLINE + 0)) && echo "num" || echo "not num"`
  if [ "$var" = "num" ]
  then
    if [ "$FORMATEDLINE" -le "$DAYSAGO" ]
    then
      TODELETE=`echo $LINE | awk '{ print $3 }'`
      rm -rf /srv/lib/elasticsearch/nodes/0/indices/$TODELETE/*
    fi
  fi
done
echo "Данные закрытых индексов удалены"
