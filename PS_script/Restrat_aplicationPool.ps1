$service_name = "Soap.Service"

$ES = Get-Process -Name w3wp -IncludeUserName | Foreach { $_.UserName } | Select-String -Pattern $service_name

Stop-WebAppPool -Name Soap.Service

While ( $ES -ne $null )
{
    Start-Sleep -s 10
    Start-WebAppPool -Name Soap.Service
    $ES = Get-Process -Name w3wp -IncludeUserName | Foreach { $_.UserName } | Select-String -Pattern $service_name
}