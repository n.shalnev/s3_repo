#! /bin/sh


host="zabbix_server"

function toZabbix ()
{
    zabbix_sender -z$host -s"MTS Subscriptions" -k$1 -o$2
}

function sdpTariffication_success {
    value=`curl -s -X GET "$1:$2/_search" -H 'Content-Type: application/json' -d'{"size": 1000, "query": {"bool": {"must": [{"match_phrase": {"Message": "Tariffication success"}},{"match": {"Application": "Sdp.Tariffication.Api"}},{"range" : {"@timestamp" : {"gte" : "now-1m/m", "lt" :  "now/m"}}}]}}}' | grep "Tariffication success" | jq '[.hits.hits[]._source.Message]' | wc -l`
    key="sdp.tariffication_success"
    toZabbix $key $value
}

function sdpTariffication_fail {
    value=`curl -s -X GET "$1:$2/_search" -H 'Content-Type: application/json' -d'{"size": 1000, "query": {"bool": {"must": [{"match_phrase": {"Message": "Tariffication fail"}},{"match": {"Application": "Sdp.Tariffication.Api"}},{"range" : {"@timestamp" : {"gte" : "now-1m/m", "lt" :  "now/m"}}}]}}}' | grep "Tariffication fail" | jq '[.hits.hits[]._source.Message]' | wc -l`
    key="sdp.tariffication_fail"
    toZabbix $key $value
}

function sdpTariffication_system_error {
    value=`curl -s -X GET "$1:$2/_search" -H 'Content-Type: application/json' -d'{"size": 1000, "query": {"bool": {"must": [{"match_phrase": {"Message": "SystemError"}},{"match": {"Application": "Sdp.Tariffication.Api"}},{"range" : {"@timestamp" : {"gte" : "now-1m/m", "lt" :  "now/m"}}}]}}}' | grep "SystemError" | jq '[.hits.hits[]._source.Message]' | wc -l`
    key="sdp.tariffication_system_error"
    toZabbix $key $value
}

function sdpTariffication_communication_error {
    value=`curl -s -X GET "$1:$2/_search" -H 'Content-Type: application/json' -d'{"size": 1000, "query": {"bool": {"must": [{"match_phrase": {"Message": "CommunicationError"}},{"match": {"Application": "Sdp.Tariffication.Api"}},{"range" : {"@timestamp" : {"gte" : "now-1m/m", "lt" :  "now/m"}}}]}}}' | grep "CommunicationError" | jq '[.hits.hits[]._source.Message]' | wc -l`
    key="sdp.tariffication_communication_error"
    toZabbix $key $value
}

function sdpTariffication_unknown_code {
    value=`curl -s -X GET "$1:$2/_search" -H 'Content-Type: application/json' -d'{"size": 1000, "query": {"bool": {"must": [{"match_phrase": {"Message": "UnknownCode"}},{"match": {"Application": "Sdp.Tariffication.Api"}},{"range" : {"@timestamp" : {"gte" : "now-1m/m", "lt" :  "now/m"}}}]}}}' | grep "UnknownCode" | jq '[.hits.hits[]._source.Message]' | wc -l`
    key="sdp.tariffication_unknown_code"
    toZabbix $key $value
}

function sdpTariffication_level_error {
    value=`curl -s -X GET "$1:$2/_search" -H 'Content-Type: application/json' -d'{"size": 1000, "query": {"bool": {"must": [{"match_phrase": {"Level": "Error"}},{"match": {"Application": "Sdp.Tariffication.Api"}},{"range" : {"@timestamp" : {"gte" : "now-1m/m", "lt" :  "now/m"}}}]}}}' | grep "Error" | jq '[.hits.hits[]._source.Message]' | wc -l`
    key="sdp.tariffication_level_error"
    toZabbix $key $value
}

sdpTariffication_success $1 $2
sdpTariffication_fail $1 $2
sdpTariffication_system_error $1 $2
sdpTariffication_communication_error $1 $2
sdpTariffication_unknown_code $1 $2
sdpTariffication_level_error $1 $2

exit 0